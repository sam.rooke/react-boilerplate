import React from 'react';

const WelcomeMessage: React.FC = () => (
    <div className="welcome">Welcome</div>
);

export default WelcomeMessage;