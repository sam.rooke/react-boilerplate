import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import RouteConstants from './RouteConstants';

import Layout from '../components/Layout/Layout';

import Home from '../pages/Home/Home';

const Router: React.FC = () => {
    return (
        <BrowserRouter>
            <Layout>
                <Route exact path={RouteConstants.Home} component={Home} />
            </Layout>
        </BrowserRouter>
    )
}

export default Router;