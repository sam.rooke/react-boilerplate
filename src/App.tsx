import React from 'react';
import logo from './logo.svg';
import './styles/App.scss';
import Router from './routes/Router';

const App: React.FC = () => {
  return (
    <div className="App">
      <Router />
    </div>
  );
}

export default App;